﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public int health;
    public int damage;
	
    public void GetHit(int damage)
    {
        health -= damage;
        Debug.Log("Current health: " + health);

        if (health < 1)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            GetHit(damage);
        }
    }
}
